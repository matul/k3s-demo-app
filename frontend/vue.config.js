const format = require('string-format');
const fs = require('fs');
const process = require('process');

module.exports = {
  transpileDependencies: ['vuex-module-decorators'],
  devServer: {
    before (app) {
      app.get('/runtime-config.json', (req, res) => {
        fs.readFile('./runtime-config.json', 'utf8', (err, config) => {
          res.setHeader('Content-Type', 'application/json');
          res.setHeader('Cache-Control', 'no-cache');
          res.write(format(config, process.env));
          res.end();
        });
      });
    },
    host: '0.0.0.0',
    hot: true,
    disableHostCheck: true,
  },
}
