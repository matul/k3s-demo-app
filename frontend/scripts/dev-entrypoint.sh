#!/bin/sh

if [ ! -d node_modules ]; then
  npm install
fi

npm run serve &
pid="$!"
trap "echo 'Stopping PID $pid'; kill -s TERM $pid" INT TERM
wait $pid

