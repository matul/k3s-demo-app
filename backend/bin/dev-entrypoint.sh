#!/bin/sh

if [ ! -f ./vendor/autoload.php ]; then
  composer install
fi

if [ ! -f ./var/data.db ]; then
  ./bin/console doctrine:schema:create
else
  ./bin/console doctrine:schema:update --force
fi

./bin/console server:run 0.0.0.0 &
pid="$!"
trap "echo 'Stopping PID $pid'; kill -s TERM $pid" SIGINT SIGTERM
wait $pid

