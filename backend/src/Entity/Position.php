<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Position
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid_binary")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $content;

    /**
     * @ORM\Column(name="position_order", type="integer")
     */
    private $order;

    public function __construct(string $id, string $content, int $order)
    {
        $this->id = $id;
        $this->content = $content;
        $this->order = $order;
    }

    public function isAfter(Position $position)
    {
        return $position->getOrder() < $this->getOrder();
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function getData()
    {
        return [
            'id' => $this->id,
            'content' => $this->content
        ];
    }
}
