<?php

namespace App\Controller;

use App\Entity\Position;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PositionController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/positions", methods={"POST"})
     */
    public function addAction(Request $request): Response
    {
        $data = json_decode($request->getContent());
        $position = new Position(
            $data->id,
            $data->content,
            $data->order
        );

        $this->em->persist($position);
        $this->em->flush();

        return new Response('', 201);
    }

    /**
     * @Route("/positions", methods={"GET"})
     */
    public function getAllAction(): Response
    {
        $positions = $this->em->getRepository(Position::class)->findAll();
        usort($positions, function (Position $a, Position $b) {
            return $a->isAfter($b);
        });

        return new JsonResponse(array_map(function (Position $position) {
            return $position->getData();
        }, $positions));
    }
}
